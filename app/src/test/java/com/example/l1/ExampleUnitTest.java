package com.example.l1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void Min1() { assertEquals(MainActivity.Min(2,1),1);}
    @Test
    public void Min2() { assertEquals(MainActivity.Min(5,10),5);}
    @Test
    public void Min3() { assertEquals(MainActivity.Min(-2,11),-2);}
    @Test
    public void Min4() { assertEquals(MainActivity.Min(-3,11),-3);}

    @Test
    public void Max1() { assertEquals(MainActivity.Max(2,1),2);}
    @Test
    public void Max2() { assertEquals(MainActivity.Max(5,10),10);}
    @Test
    public void Max3() { assertEquals(MainActivity.Max(-2,11),11);}
    @Test
    public void Max4() { assertEquals(MainActivity.Max(-2,22),22);}
}