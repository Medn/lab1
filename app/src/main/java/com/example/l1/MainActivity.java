package com.example.l1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;



public class MainActivity extends AppCompatActivity {

    /**
     * @param a integer
     * @param b integer
     * @return Min of a and b
     *
     */
    public  static int Min(int a, int b){
        return a < b ? a : b;
    }
    /**
     * @param a integer
     * @param b integer
     * @return Max of a and b
     *
     */
    public static int Max(int a,int b){
        return a < b ? b : a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}